package com.formacion.apirest.service;

import java.util.List;

import com.formacion.apirest.entity.Producto;

public interface ProductoService {
	
	//metodo para mostrar todos los Productos
	public List<Producto> mostrarProductos();

	//metodo que busque Productos por id
	public Producto buscarProducto(long id);
	
	//metodo para crear un nuevo Producto
	public Producto guardarProducto(Producto producto);
	
	//metodo para borrar un Producto
	public Producto borrarProducto(long id);
	
	

	

}
